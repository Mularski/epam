class Coordinates:
    def __init__(self, X, Y):
        super().__init__()
        self.X = X
        self.Y = Y

    def __repr__(self):
        return f"({self.X} - {self.Y})"


class NumberFinder:
    vowels = ('A', 'E', 'I', 'O', 'U')

    board = [
        ['A', 'B', 'C', 'D', 'E'],
        ['F', 'G', 'H', 'I', 'J'],
        ['K', 'L', 'M', 'N', 'O'],
        ['', '1', '2', '3', '']
    ]

    def __init__(self):
        super().__init__()

        self.possible_results = []

        self.X = [2, 1, -1, -2, -2, -1, 1, 2]
        self.Y = [1, 2, 2, 1, -1, -2, -2, -1]

        self.possible_combinations = [Coordinates(self.X[idx], self.Y[idx]) for idx in range(0,len(self.X))]

    def number_of_vowels(self, sequence):
        count = 0
        for key in sequence:
            count += 1 if key.upper() in self.vowels else 0
        return count

    def find_all_sequences(self, start_step):
        if not self._check_if_key_in_board(start_step):
            raise Exception("Key {start_step} is not on the keyboard")

        self.possible_results = []
        self._find_sequence(start_step, [])
        print(self.possible_results)
        print(len(self.possible_results))

    def _check_if_key_in_board(self, key):
        return next((True for row in self.board if key in row), False)

    def _find_coordinates_of_curent_step(self, current_key):
        x, y = next(((row.index(current_key), y) for y, row in enumerate(self.board) if current_key in row), None)
        if x is None or y is None:
            raise Exception(f'There is no such key as {current_key} on the keyboard')
        return x, y

    @staticmethod
    def _create_or_append(current_key, sequence):
        if not sequence:
            return [current_key]
        else:
            sequence.append(current_key)
            return sequence

    def validate_sequence(self, sequence, current_key):        
        if self.number_of_vowels(sequence) > 2:
            return False

        if len(sequence) >= 10:
            self.possible_results.append(sequence) if sequence not in self.possible_results else None
            return False

        if sequence and current_key == sequence[-1]:
            return False

        return True
   
    def _find_sequence(self, current_key, sequence):
      
        if not self.validate_sequence(sequence, current_key) or not current_key:
            return
        
        sequence = self._create_or_append(current_key, sequence)

        for combination in self.possible_combinations: 
            try:
                x, y = self._find_coordinates_of_curent_step(current_key)
                next_step = self.board[combination.Y + y][combination.X + y]
                self._find_sequence(next_step, [i for i in sequence])
            except IndexError:
                return


if __name__ == '__main__':
    finder = NumberFinder()
    finder.find_all_sequences('A')
