import unittest
from script import NumberFinder


class NumberFinderTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.finder = NumberFinder()
        return super().setUpClass()
 
    def test_number_of_possible_combinations(self):
        expected = 20
        self.finder.find_all_sequences('A')
        result = len(self.finder.possible_results)
        self.assertEqual(expected, result)

    def test_number_of_vowels(self):
        expected = 3
        result = self.finder.number_of_vowels(['A', 'E', 'A'])
        self.assertEqual(expected, result)

    def test_too_high_number_of_vowels__should_stop_execution(self):
        result = self.finder._find_sequence('A', ['A', 'E', 'A'])
        self.assertEqual(result, None)

    def test_sequence_length_validation__should_stop_execution(self):
        sequence = ['A'] * 10
        result = self.finder._find_sequence('A', sequence)
        self.assertEqual(result, None)
    
    def test_find_sequence_no_key__should_stop_execution(self):
        sequence = ['A']
        key = ''
        result = self.finder._find_sequence(key, sequence)
        self.assertEqual(result, None)

    def test_find_all_sequences_invalid_key__should_throw_exception(self):
        start_key = 'X'
        self.assertRaises(Exception, self.finder.find_all_sequences, start_key)

